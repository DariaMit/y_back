conn = psycopg2.connect(dbname=db_name, user=db_user, 
                        password=db_password, host='localhost')


def add_item_to_db(item, update_date):
    with conn.cursor() as cur:
        conn.autocommit = True
        cur.execute('INSERT INTO "schema".offers (id, "name", parentID, price, "date") \
        VALUES (%(id)s, %(name)s, %(parentId)s, %(price)s, %(date)s) \
        ON CONFLICT (id) DO UPDATE SET \
        "name"=%(name)s, \
        parentId=%(parentId)s, \
        price=%(price)s, \
        "date"=%(date)s \
        WHERE "schema".offers.id=%(id)s;',
        {'id': item["id"], 'name': item["name"], 'parentId': item["parentId"], 'price': item["price"], 'date': update_date})
        


def add_cat_to_db(cat, update_date):
    with conn.cursor() as cur:
        conn.autocommit = True
        cur.execute('INSERT INTO "schema".categories (id, "name", parentID, "date") \
        VALUES (%(id)s, %(name)s, %(parentId)s, %(date)s) \
        ON CONFLICT (id) DO UPDATE SET \
        "name"=%(name)s, \
        parentId=%(parentId)s, \
        "date"=%(date)s \
        WHERE "schema".categories.id=%(id)s;',
        {'id': cat["id"], 'name': cat["name"], 'parentId': cat["parentId"], 'date': update_date})


def get_item_from_db(item_id):
'''ну запуталась я ну запуталась!!!!'''

    def get_item_children(items:json, cur):
        if len(items) == 0: #проверить не пустой ли джейсон???
            return None
        for i in range(len(items)):
            id = items[i].get('id') #находим id каждого элемента из джейсона??
            children = []

            cur.execute('SELECT * from "schema".categories WHERE parentid=%(id)s', #хорошо бы тут сразу джейсон возвращать
            {'id': item_id})
            try:
                children_cats_list = cur.fetchall() #найдём все дочерние категории, допустим, они все в джейсоне уже
                for cat in children_cats_list:
                    children.append(cat)
            except Exception:
                print(Exception)
                # children_ids = []

            cur.execute('SELECT * from "schema".offers WHERE parentid=%(id)s', #хорошо бы тут сразу джейсон возвращать
            {'id': item_id})
            try:
                children_offers_list = cur.fetchall() #найдём все дочерние категории, допустим, они все в джейсоне уже
                for offer in children_offers_list:
                    children.append(offer)
            except Exception:
                print(Exception)

            items[i]['children']= get_item_children(children, cur)
        return items


    with conn.cursor as cur:
        res_offer = cur.execute('SELECT id FROM "schema".offers WHERE id=%(id)s', 
        {"id": item_id})
        if res_offer:
            cur.execute('SELECT id FROM "schema".offers WHERE id=%(id)s', 
            {"id": item_id})
            whole_item = cur.fethcone()
            return get_item_children(whole_item)



def delete_item_from_db(item_id):

    def find_children_cats(items:list, cur):
        if len(items) == 0:
            return []
        children_to_delete = []
        for i in items:
            cur.execute('SELECT id FROM "schema".categories WHERE parentid=%(id)s', 
            {'id': item_id})
            try:
                children = cur.fetchall()
                children_ids = list(map(lambda x:x[0], children))
            except Exception:
                print(Exception)
                children_ids = []
            finally:
                children_to_delete += find_children_cats(children_ids, cur)
                children_to_delete.append(i)
                return children_to_delete


    with conn.cursor() as cur:
        to_delete = find_children_cats([item_id], cur)
        conn.autocommit = True
        for item in to_delete:
            cur.execute('DELETE FROM "schema".offers WHERE parentid=%(id)s', {'id': item})
            cur.execute('DELETE FROM "schema".offers WHERE id=%(id)s', {'id': item})
            cur.execute('DELETE FROM "schema".categories WHERE id=%(id)s', {'id': item})



