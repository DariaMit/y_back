from flask import Flask, request, jsonify
import psycopg2
import json
import re

app = Flask(__name__)

db_password = 'admin'
db_user = 'postgres'
db_name = 'y_back'


responses = {
    400: app.response_class(
        response='{ "code": 400, "message": "Validation Failed" }\n',
        status=400,
        mimetype='application/json'
    ),
    404: app.response_class(
        response='{ "code": 404, "message": "Item not found" }\n',
        status=404,
        mimetype='application/json'
    )
}


conn = psycopg2.connect(dbname=db_name, user=db_user, 
                        password=db_password, host='y_back-postgres-1')
print('connection established')


@app.route("/imports", methods=["POST"])
def add_item():
    if not request.is_json:
        return responses[400]
    item = request.get_json()
    update_date = item["updateDate"]

    if not is_date_valid(update_date):
        return responses[400]
    for i in item["items"]:
        if not is_item_valid(i):
            return responses[400]
        if i["type"] == "OFFER":
            add_item_to_db(i, update_date)
        elif i["type"] == "CATEGORY":
            add_cat_to_db(i, update_date)
    return "OK", 200


@app.route("/delete/<string:id>", methods=["DELETE"])
def delete_item(id):
    if not is_id_valid(id):
        return responses[400]
    result = delete_items_from_db(id)
    if not result:
        return responses[404]
    return "OK", 200



@app.get("/nodes/<string:id>")
def get_item_info(id):
    item_id = id
    if not is_id_valid(id):
        return responses[400]
    with conn.cursor() as cur:
        result = recursive_tree(id, cur)[0]
        if not result:
            return responses[404]
    return jsonify(result), 200


def add_item_to_db(item, update_date):
    with conn.cursor() as cur:
        cur.execute('INSERT INTO "schema".offers (id, "name", "parentId", price, "date") \
        VALUES (%(id)s, %(name)s, %("parentId")s, %(price)s, %(date)s) \
        ON CONFLICT (id) DO UPDATE SET \
        "name"=%(name)s, \
        "parentId"=%("parentId")s, \
        price=%(price)s, \
        "date"=%(date)s \
        WHERE "schema".offers.id=%(id)s;',
        {'id': item["id"], 'name': item["name"], '"parentId"': item["parentId"], 'price': item["price"], 'date': update_date})

        cur.execute('''WITH RECURSIVE t1(id, "parentId") AS (SELECT id, "parentId" FROM "schema".offers \
                    WHERE id=%(id)s \
                    UNION \
                    SELECT t.id, t."parentId" FROM "schema".categories t JOIN t1 ON t1."parentId" = t.id) \
                    UPDATE "schema".categories SET \
                    "date" = %(date)s \
                    WHERE id IN (select id from t1)''',
                    {'id': item["id"], 'date': update_date})

        conn.commit()



def add_cat_to_db(cat, update_date):
    with conn.cursor() as cur:
        cur.execute('INSERT INTO "schema".categories (id, "name", "parentId", "date") \
        VALUES (%(id)s, %(name)s, %("parentId")s, %(date)s) \
        ON CONFLICT (id) DO UPDATE SET \
        "name"=%(name)s, \
        "parentId"=%("parentId")s, \
        "date"=%(date)s \
        WHERE "schema".categories.id=%(id)s;',
        {'id': cat["id"], 'name': cat["name"], '"parentId"': cat["parentId"], 'date': update_date})

        cur.execute('''WITH RECURSIVE t1(id, "parentId") AS (SELECT id, "parentId" FROM "schema".categories \
                    WHERE id=%(id)s \
                    UNION \
                    SELECT t.id, t."parentId" FROM "schema".categories t JOIN t1 ON t1."parentId" = t.id) \
                    UPDATE "schema".categories SET \
                    "date" = %(date)s \
                    WHERE id IN (select id from t1)''',
                    {'id': cat["id"], 'date': update_date})
        conn.commit()




def is_item_valid(data):
    if not data["type"] in ["OFFER", "CATEGORY"]:
        print(1)
        return 1, False
    if data.get("type") == "OFFER":
        if not data.get("price"):
            print(2)
            return 2, False
        if not isinstance(data.get("price"), (int, float)):
            print(3)
            return 3, False
    if not data.get("name"):
        print(4)
        return 4, False
    if data.get("type") == "CATEGORY" and data.get("price"):
        print(5)
        return 5, False
    return True


def is_date_valid(date):
    if re.match(r'^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$', date) is not None:
        return True
    return False

def is_id_valid(id):
    if re.match(r'^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$', id) is not None:
        return True
    return False


def recursive_tree(id, cur):
    cur.execute('SELECT row_to_json(offers) FROM "schema".offers WHERE id=%(id)s', {'id': id})
    res = cur.fetchone()
    if not res:
            cur.execute('SELECT row_to_json(categories) FROM "schema".categories WHERE id=%(id)s', {'id': id})
            res = cur.fetchone()
            if not res:
                return [], []
            res[0]["type"] = "CATEGORY"
    else:
        res[0]["type"] = "OFFER"
        res[0]["price"] = int(res[0].get('price'))
    item = res[0]
    children = []
    cur. execute('SELECT id FROM "schema".offers WHERE "parentId"=%(id)s', {'id': id})
    offers_children = cur.fetchall()
    if offers_children:
        for offer in offers_children:
            children.append(offer[0])
    cur. execute('SELECT id FROM "schema".categories WHERE "parentId"=%(id)s', {'id': id})
    cats_children = cur.fetchall()
    if cats_children:
        for cat in cats_children:
            children.append(cat[0])
    item["children"] = []
    cnt = []
    for child in children:
        result = recursive_tree(child, cur)
        item["children"].append(result[0])
        cnt.append(result[1])
        
    if not children:
        item["children"] = None
    all_prices = 0
    if item["children"]:
        summ = 0
        for i in range(len(item["children"])):
            all_prices += item["children"][i].get("price", 0) * cnt[i]
            summ += cnt[i]
    else:
        summ = 1
    item["price"] = item.get("price", all_prices // summ)
    return item, summ


def delete_items_from_db(item_id):
    with conn.cursor() as cur:
        cur.execute('SELECT * FROM "schema".categories WHERE id=%(id)s', {'id': item_id})
        res_off = cur.fetchone()
        if not res_off:
            cur.execute('SELECT * FROM "schema".offers WHERE id=%(id)s', {'id': item_id})
            res_cat = cur.fetchone()
            if not res_cat:
                return False
        cur.execute('''WITH RECURSIVE t1(id, "parentId") AS (SELECT id, "parentId" FROM "schema".categories WHERE id=%(id)s
                    UNION
                    SELECT t.id, t."parentId" FROM "schema".categories t JOIN t1 ON t1.id = t."parentId")
                    DELETE FROM "schema".offers WHERE "parentId" IN (SELECT id FROM t1) OR id=%(id)s''', 
                    {'id': item_id})
        cur.execute('''WITH RECURSIVE t1(id, "parentId") AS (SELECT id, "parentId" FROM "schema".categories WHERE id=%(id)s
                    UNION
                    SELECT t.id, t."parentId" FROM "schema".categories t JOIN t1 ON t1.id = t."parentId")
                    DELETE FROM "schema".categories WHERE "parentId" IN (SELECT id FROM t1) OR id=%(id)s''', 
                    {'id': item_id})
        conn.commit()
        return True
        
