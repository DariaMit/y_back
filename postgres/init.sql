CREATE SCHEMA "schema";   

CREATE TABLE "schema".categories ( 
    id varchar NOT NULL, 
    "parentId" varchar NULL, 
    "name" varchar NOT NULL, 
    "date" varchar NOT NULL,
    CONSTRAINT offers_pk PRIMARY KEY (id) 
); 

CREATE TABLE "schema".offers ( 
    id varchar NOT NULL, 
    "parentId" varchar NULL, 
    "name" varchar NOT NULL, 
    price integer NOT NULL, 
    "date" varchar NOT NULL,
    CONSTRAINT cats_pk PRIMARY KEY (id), 
    FOREIGN KEY ("parentId") REFERENCES "schema".categories (id) ON DELETE CASCADE ON UPDATE CASCADE 
);